import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { studentService, subjectService, combineService } from './services';
import { Card, List, Row, Column, NavBar, Button, Form } from './widgets';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

class Menu extends Component {
  render() {
    return (
      <NavBar brand="WhiteBoard">
        <NavBar.Link to="/students">Students</NavBar.Link>
        <NavBar.Link to="/subjects">Subjects</NavBar.Link>
      </NavBar>
    );
  }
}

class Home extends Component {
  render() {
    return <Card title="Welcome">Welcome to WhiteBoard</Card>;
  }
}

class StudentList extends Component {
  students = [];

  render() {
    return (
      <Card title="Students">
        <List>
          {this.students.map(student => (
            <List.Item key={student.id} to={'/students/' + student.id}>
              {student.name}
            </List.Item>
          ))}
        </List>
        <Row>
          <Button.Light onClick={this.add}>Add new student</Button.Light>
        </Row>
      </Card>
    );
  }

  mounted() {
    studentService.getStudents(students => {
      this.students = students;
    });
  }

  add() {
    history.push("/students/create");
  }
}

class StudentDetails extends Component {
  student = null;
  subjects = [];

  render() {
    if (!this.student) return null;

    return (
      <div>
        <Card title="Student details">
          <Row>
            <Column width={2}>Name:</Column>
            <Column>{this.student.name}</Column>
          </Row>
          <Row>
            <Column width={2}>Email:</Column>
            <Column>{this.student.email}</Column>
          </Row>
          <Row>
            <Column width={2}>Subjects:</Column>
            <Column>
              <List>
                {this.subjects.map(subject => (
                  <List.Item key={subject.id} to={"/subjects/" + subject.id}>
                    {subject.name}
                  </List.Item>
                ))}
              </List>
            </Column>
          </Row>
        </Card>
        <Row>
          <Column>
        <Button.Light widht={3} onClick={this.edit}>Edit</Button.Light>
          </Column>
          <Column>
            <Button.Light width={3} onClick={this.signUp}>New subject</Button.Light>
          </Column>
          <Column>
            <Button.Light width={3} onClick={this.resign}>Remove subject</Button.Light>
          </Column>
          <Column>
        <Button.Danger width={3} onClick={this.delete}>Remove student</Button.Danger>
          </Column>
        </Row>
      </div>
    );
  }

  mounted() {
    studentService.getStudent(this.props.match.params.id, student => {
      this.student = student;
    });
    studentService.getSubjects(this.props.match.params.id, subjects => {
      this.subjects = subjects;
    });
  }

  edit() {
    history.push('/students/' + this.student.id + '/edit');
  }

  signUp() {
    history.push("/students/" + this.student.id + "/signup")
  }

  resign() {
    history.push("/students/" + this.student.id + "/resign")
  }

  delete() {
    history.push("/students/" + this.student.id + "/remove");
  }
}

class StudentEdit extends Component {
  student = null;

  render() {
    if (!this.student) return null;

    return (
      <div>
        <Card title="Edit student">
          <Form.Label>Name:</Form.Label>
          <Form.Input type="text" value={this.student.name} onChange={e => (this.student.name = e.target.value)} />
          <Form.Label>Email:</Form.Label>
          <Form.Input type="text" value={this.student.email} onChange={e => (this.student.email = e.target.value)} />
        </Card>
        <Row>
          <Column>
            <Button.Success onClick={this.save}>Save</Button.Success>
          </Column>
          <Column right>
            <Button.Light onClick={this.cancel}>Cancel</Button.Light>
          </Column>
        </Row>
      </div>
    );
  }

  mounted() {
    studentService.getStudent(this.props.match.params.id, student => {
      this.student = student;
    });
  }

  save() {
    studentService.updateStudent(this.student, () => {
      history.push('/students/' + this.props.match.params.id);
    });
  }

  cancel() {
    history.push('/students/' + this.props.match.params.id);
  }
}

class StudentAdd extends Component {
  student = {};

  render() {

    return (
      <div>
        <Card title="Add new student">
          <Form.Label>Name:</Form.Label>
          <Form.Input type="text" onChange={e => (this.student.name = e.target.value)} />
          <Form.Label>Email:</Form.Label>
          <Form.Input type="text" onChange={e => (this.student.email = e.target.value)} />
        </Card>
        <Row>
          <Column>
            <Button.Success onClick={this.save}>Save</Button.Success>
          </Column>
          <Column right>
            <Button.Danger onClick={this.cancel}>Cancel</Button.Danger>
          </Column>
        </Row>
      </div>
    );
  }

  save() {
    studentService.addStudent(this.student, () => {
      history.push('/students/');
    });
  }

  cancel() {
    history.push('/students/');
  }
}

class StudentRemove extends Component {
  studentName = null;

  render() {
    return(
      <Card title={"Are you sure you want to remove " + this.studentName + "?"}>
        <Row>
          <Column>
            <Button.Danger onClick={this.confirmRemove}>Yes</Button.Danger>
          </Column>
          <Column right>
            <Button.Light onClick={this.back}>Back</Button.Light>
          </Column>
        </Row>
      </Card>
    )
  }

  mounted() {
    studentService.getStudent(this.props.match.params.id, student => {
      this.studentName = student.name;
    });
  }

  confirmRemove() {
    studentService.deleteStudent(this.props.match.params.id, () => {
      history.push("/students");
    });
  }

  back() {
    history.push("/students/" + this.props.match.params.id);
  }
}

class SubjectList extends Component {
  subjects = [];

  render() {
    return (
      <Card title="Subjects">
        <List>
          {this.subjects.map(subject => (
            <List.Item key={subject.id} to={'/subjects/' + subject.id}>
              {subject.name}
            </List.Item>
          ))}
        </List>
        <Row>
          <Button.Light onClick={this.add}>Add new subject</Button.Light>
        </Row>
      </Card>
    );
  }

  mounted() {
    subjectService.getSubjects(subjects => {
      this.subjects = subjects;
    });
  }

  add() {
    history.push("/subjects/create");
  }
}

class SubjectDetails extends Component {
  subject = null;
  students = [];

  render() {
    if (!this.subject) return null;

    return (
      <div>
        <Card title="Subject details">
          <Row>
            <Column width={2}>Name:</Column>
            <Column>{this.subject.name}</Column>
          </Row>
          <Row>
            <Column width={2}>Code:</Column>
            <Column>{this.subject.code}</Column>
          </Row>
          <Row>
            <Column width={2}>Participating students:</Column>
            <Column>
              <List>
                {this.students.map(student => (
                  <List.Item key={student.id} to={"/students/" + student.id}>
                    {student.name}
                  </List.Item>
                ))}
              </List>
            </Column>
          </Row>
        </Card>
        <Row>
          <Column>
            <Button.Light onClick={this.edit}>Edit</Button.Light>
          </Column>
          <Column right>
            <Button.Danger onClick={this.delete}>Remove subject</Button.Danger>
          </Column>
        </Row>
      </div>
    );
  }

  mounted() {
    subjectService.getSubject(this.props.match.params.id, subject => {
      this.subject = subject;
    });
    subjectService.getStudents(this.props.match.params.id, students => {
      this.students = students;
    });
  }

  edit() {
    history.push('/subjects/' + this.subject.id + '/edit');
  }

  delete() {
    history.push("/subjects/" + this.subject.id + "/remove");
  }
}

class SubjectEdit extends Component {
  subject = null;

  render() {
    if (!this.subject) return null;

    return (
      <div>
        <Card title="Edit subject">
          <Form.Label>Name:</Form.Label>
          <Form.Input type="text" value={this.subject.name} onChange={e => (this.subject.name = e.target.value)} />
          <Form.Label>Code:</Form.Label>
          <Form.Input type="text" value={this.subject.code} onChange={e => (this.subject.code = e.target.value)} />
        </Card>
        <Row>
          <Column>
            <Button.Success onClick={this.save}>Save</Button.Success>
          </Column>
          <Column right>
            <Button.Light onClick={this.cancel}>Cancel</Button.Light>
          </Column>
        </Row>
      </div>
    );
  }

  mounted() {
    subjectService.getSubject(this.props.match.params.id, subject => {
      this.subject = subject;
    });
  }

  save() {
    subjectService.updateSubject(this.subject, () => {
      history.push('/subjects/' + this.props.match.params.id);
    });
  }

  cancel() {
    history.push('/subjects/' + this.props.match.params.id);
  }
}

class SubjectAdd extends Component {
  subject = {};

  render() {

    return (
      <div>
        <Card title="Add new subject">
          <Form.Label>Name:</Form.Label>
          <Form.Input type="text" onChange={e => (this.subject.name = e.target.value)} />
          <Form.Label>Code:</Form.Label>
          <Form.Input type="text" onChange={e => (this.subject.code = e.target.value)} />
        </Card>
        <Row>
          <Column>
            <Button.Success onClick={this.save}>Save</Button.Success>
          </Column>
          <Column right>
            <Button.Danger onClick={this.cancel}>Cancel</Button.Danger>
          </Column>
        </Row>
      </div>
    );
  }

  save() {
    subjectService.addSubject(this.subject, () => {
      history.push('/subjects');
    });
  }

  cancel() {
    history.push('/subjects');
  }
}

class SubjectRemove extends Component {
  subjectName = null;

  render() {
    return(
      <Card title={"Are you sure you want to remove " + this.subjectName + "?"}>
        <Row>
          <Column>
            <Button.Danger onClick={this.confirmRemove}>Yes</Button.Danger>
          </Column>
          <Column right>
            <Button.Light onClick={this.back}>Back</Button.Light>
          </Column>
        </Row>
      </Card>
    )
  }

  mounted() {
    subjectService.getSubject(this.props.match.params.id, subject => {
      this.subjectName = subject.name;
    });
  }

  confirmRemove() {
    subjectService.deleteSubject(this.props.match.params.id, () => {
      history.push("/subjects");
    });
  }

  back() {
    history.push("/subjects/" + this.props.match.params.id);
  }
}

class SignUp extends Component {
  availableSubjects = [];
  selectedSubjects = [];
  full = false;

  render() {
    if (!this.full) {
      return (
        <Card title="Select subjects to sign up for">
          <List>
            {this.availableSubjects.map(subject => (
              <List.Item key={subject.id}>
                <Row>
                  <Column width={10}>
                    <Form.Label htmlFor={subject.id}>{subject.name + " " + subject.code}</Form.Label>
                  </Column>
                  <Column>
                    <Form.Input className="form-check-input" type="checkbox" id={subject.id}
                                onChange={event => this.handleCheck(event)}/>
                  </Column>
                </Row>
              </List.Item>
            ))}
          </List>
          <Row>
            <Column>
              <Button.Success onClick={this.signUp}>Sign up</Button.Success>
            </Column>
            <Column right>
              <Button.Light onClick={this.cancel}>Cancel</Button.Light>
            </Column>
          </Row>
        </Card>
      )
    } else {
      return(
        <div>
          <h6>Student is already taking all available courses</h6>
          <p>Redirecting home...</p>
        </div>
      )
    }
  }

  handleCheck(event) {
    if (event.target.checked) {
      this.selectedSubjects.push(event.target.id);
    } else {
      let index = this.selectedSubjects.indexOf(event.target.id);
      if (index > -1) {
        this.selectedSubjects.splice(index, 1);
      }
    }
  }

  signUp() {
    for (let i = 0; i < this.selectedSubjects.length; i++) {
      let subjId = this.selectedSubjects[i];
      combineService.addStudentSubject(this.props.match.params.id, subjId, () => {
      });
    }
    history.push("/students/" + this.props.match.params.id);
  }

  cancel() {
    history.push("/students/" + this.props.match.params.id);
  }

  mounted() {
    combineService.getAvailableSubjects(this.props.match.params.id, subjects => {
      this.availableSubjects = subjects;
      if (subjects.length === 0) {
        this.full = true;
        setTimeout(function() {
          history.push("/students");
        }, 3000);
      }
    });
  }
}

class Resign extends Component {
  subjects = [];
  selectedSubjects = [];
  empty = false;

  render() {
    if (!this.empty) {
      return (
        <Card title="Select subjects to resign from">
          <List>
            {this.subjects.map(subject => (
              <List.Item key={subject.id}>
                <Row>
                  <Column width={10}>
                    <Form.Label htmlFor={subject.id}>{subject.name + " " + subject.code}</Form.Label>
                  </Column>
                  <Column>
                    <Form.Input className="form-check-input" type="checkbox" id={subject.id}
                                onChange={event => this.handleCheck(event)}/>
                  </Column>
                </Row>
              </List.Item>
            ))}
          </List>
          <Row>
            <Column>
              <Button.Danger onClick={this.resign}>Resign</Button.Danger>
            </Column>
            <Column right>
              <Button.Light onClick={this.cancel}>Cancel</Button.Light>
            </Column>
          </Row>
        </Card>
      )
    } else {
      return(
        <div>
          <h6>Student is not taking any subjects</h6>
          <p>Redirecting home...</p>
        </div>
      )
    }
  }

  handleCheck(event) {
    if (event.target.checked) {
      this.selectedSubjects.push(event.target.id);
    } else {
      let index = this.selectedSubjects.indexOf(event.target.id);
      if (index > -1) {
        this.selectedSubjects.splice(index, 1);
      }
    }
  }

  resign() {
    for (let i = 0; i < this.selectedSubjects.length; i++) {
      let subjId = this.selectedSubjects[i];
      combineService.deleteStudentSubject(this.props.match.params.id, subjId, () => {
      });
    }
    history.push("/students/" + this.props.match.params.id);
  }

  cancel() {
    history.push("/students/" + this.props.match.params.id);
  }

  mounted() {
    studentService.getSubjects(this.props.match.params.id, subjects => {
      this.subjects = subjects;
      if (subjects.length === 0) {
        this.empty = true;
        setTimeout(function() {
          history.push("/students");
        }, 3000);
      }
    });
  }
}

ReactDOM.render(
  <HashRouter>
    <div>
      <Menu />
      <Route exact path="/" component={Home} />
      <Route exact path="/students" component={StudentList} />
      <Route exact path="/students/:id" component={StudentDetails} />
      <Route exact path="/students/:id/edit" component={StudentEdit} />
      <Route exact path="/students/:id/remove" component={StudentRemove} />
      <Route exact path="/students/create" component={StudentAdd} />
      <Route exact path="/subjects" component={SubjectList} />
      <Route exact path="/subjects/:id" component={SubjectDetails} />
      <Route exact path="/subjects/:id/edit" component={SubjectEdit} />
      <Route exact path="/subjects/:id/remove" component={SubjectRemove} />
      <Route exact path="/subjects/create" component={SubjectAdd} />
      <Route path="/students/:id/signup" component={SignUp} />
      <Route path="/students/:id/resign" component={Resign} />
    </div>
  </HashRouter>,
  document.getElementById('root')
);
